<?php
/**
 * Created by PhpStorm.
 * User: ccorfmat
 * Date: 25/03/2017
 * Time: 18:41
 */

namespace Drupal\assign_user_roles;

class AssignUserRolesPermissions {
  /**
   * Create dynamic permissions.
   *
   * @return array
   *    Permissions to create
   */
  public function dynamicPermissions() {
    $permissions = [];

    $roles = user_roles(TRUE);

    // Create permission for each role.
    foreach ($roles as $role) {
      $role_label = $role->get('label');
      $role_id = $role->get('id');
      $perm_name = 'assign_' . $role_id . '_role';
      $perm_title = t('Assign role %label to the users', array('%label' => $role_label));
      $permissions[$perm_name] = array(
        'title' => $perm_title,
      );
    }

    return $permissions;
  }
}